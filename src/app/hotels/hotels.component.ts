import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { HotelService } from '../hotel.service';
import { Hotel } from '../hotel';
import { AvailabilityModalComponent } from '../availability-modal/availability-modal.component'

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.scss']
})
export class HotelsComponent implements OnInit {
  hotels: Hotel[];

  constructor(private hotelService: HotelService, private modalService: NgbModal) { }

  ngOnInit() {
    this.getHotels();
  }

  open(hotel) {
    const modalRef = this.modalService.open(AvailabilityModalComponent);
    modalRef.componentInstance.hotel = hotel;
  }

  getHotels(): void {
    this.hotelService.getHotels()
        .subscribe(hotels => this.hotels = hotels);
  }
}
