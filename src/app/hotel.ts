export class Hotel {
  slug: string;
  name: string;
  image_url: string;
  room_types: RoomType[];
  available: boolean;
}

export class RoomType {
  id: string;
  kind: string;
}