import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule }    from '@angular/common/http';

import { AppComponent } from './app.component';
import { HotelsComponent } from './hotels/hotels.component';
import { AppRoutingModule } from './app-routing.module';
import { AvailabilityModalComponent } from './availability-modal/availability-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    HotelsComponent,
    AvailabilityModalComponent
  ],
  imports: [
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [AvailabilityModalComponent],
})
export class AppModule { }
