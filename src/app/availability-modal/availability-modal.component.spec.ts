import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvailabilityModalComponent } from './availability-modal.component';

describe('AvailabilityModalComponent', () => {
  let component: AvailabilityModalComponent;
  let fixture: ComponentFixture<AvailabilityModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailabilityModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailabilityModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
