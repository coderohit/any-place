import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Hotel } from '../hotel';

@Component({
  selector: 'app-availability-modal',
  templateUrl: './availability-modal.component.html',
  styleUrls: ['./availability-modal.component.scss']
})
export class AvailabilityModalComponent {
  @Input() hotel: Hotel;

  constructor(public modal: NgbActiveModal) {}

}
